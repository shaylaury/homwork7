<?php

use yii\db\Migration;

class m160529_074112_init_lead_table extends Migration
{
    public function up()
    {
            $this->createTable(
                'lead',
                [
                    'id' => 'pk',
                    'name' => 'string',
                    'birth_date' =>'date',
                    'status' => 'int',
                    'notes' => 'text',

                ],
                    'ENGINE = InnoDB'


                  );
    }

    public function down()
    {
                 $this->dropTable('lead') ;
      
    }

   
}
